sap.ui.define(["sap/ui/core/mvc/Controller", "../model/formatter"], function(Controller, Formatter) {
	"use strict";
	return Controller.extend("insp.tslZ_TSL.controller.Master", {
		formatter: Formatter,
		onInit: function() {

		},
		onAfterRendering: function() {
			this.getView().getModel().setUseBatch(false);
			this.getView().getModel().setRefreshAfterChange(false);
			this.getView().getModel("Projects").setUseBatch(false);
			this.getView().getModel("Tasks").setUseBatch(false);
			this.getView().getModel("LoggingCreate").setUseBatch(false);

		},
		onCalendarSelect: function(oEvent) {
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "YYYYMMdd"
			});
			var startDate = dateFormat.format(new Date(oEvent.getSource().getSelectedDates()[0].getStartDate()));
			var endDate = dateFormat.format(new Date(oEvent.getSource().getSelectedDates()[0].getEndDate()));
			this.getView().byId("__table0").getBinding("items").filter(new sap.ui.model.Filter("A0CALDAY", "BT", startDate, endDate));
		},
		onAddPress: function(oEvent) {
			this.getView().getModel("Projects").read("/CMO_TSPPRJResults");
			var item = new sap.ui.model.json.JSONModel();
			if (this.dialogAdd) {
				this.dialogAdd.open();
			} else {
				this.dialogAdd = sap.ui.xmlfragment("insp.tslZ_TSL.view.Add", this);
				this.dialogAdd.open();
			}
			this.dialogAdd.setModel(item, "item");
			this.dialogAdd.setModel(this.getView().getModel("Projects"), "Projects");
			this.dialogAdd.setModel(this.getView().getModel("Tasks"), "Tasks");
		},
		onIconTabBarSelect: function(oEvent) {
			console.log(oEvent.getSource().getSelectedKey());
			this.getView().byId("__table0").getBinding("items").filter(new sap.ui.model.Filter("A0EP63AIXTZ482UMWIKN25MIPD", "EQ", oEvent.getSource()
				.getSelectedKey()));
		},
		onEditPress: function(oEvent) {
			var item = new sap.ui.model.json.JSONModel(oEvent.getSource().getBindingContext().getProperty(oEvent.getSource().getBindingContext()
				.getPath()));
			if (this.dialogEdit) {
				this.dialogEdit.open();
			} else {
				this.dialogEdit = sap.ui.xmlfragment("insp.tslZ_TSL.view.Edit", this);
				this.dialogEdit.open();
			}
			this.dialogEdit.setModel(item, "item");
			this.dialogEdit.setModel(this.getView().getModel("Projects"), "Projects");
			this.dialogEdit.setModel(this.getView().getModel("Tasks"), "Tasks");
		},
		onProjectSelect: function(oEvent) {
			var prj = oEvent.getSource().getSelectedItem().getKey();
			console.log(prj);
			var TSK = new sap.ui.model.json.JSONModel();
			var that = this;
			this.dialogAdd.getModel("Tasks").read("/CMO_TSPTSK(CMO_PRJV='" + prj + "')/Results", {
				success: function(oData, response) {
					TSK = new sap.ui.model.json.JSONModel(oData.results);
					console.log(TSK);
					that.dialogAdd.setModel(TSK, "TSK");
				}
			});
			console.log(TSK);
			that.dialogAdd.setModel(TSK, "TSK");
		},
		onUpdatePress: function(oEvent) {
			var item = this.dialogEdit.getModel("item");
			var that = this;
			var model = this.getView().getModel();
			model.update("/AGH_TSHResults('" + item.oData.ID.replace(" ", "%20") + "')", item.oData, {
				"success": function() {
					console.log("success");
				},
				"error": function(error) {
					that.onErrorDialog(JSON.stringify(error));

					console.log("Fail", error);
				},
				"merge": false,
				"async": true,
				"refreshAfterChange": false
			});
			this.dialogEdit.close();
		},
		onBackPressAdd: function(oEvent) {
			this.dialogAdd.close();
		},
		onBackPressEdit: function(oEvent) {
			this.dialogEdit.close();
		},
		onDeletePress: function(oEvent) {},
		onSearch: function(oEvent) {
			var sQuery = oEvent.getSource().getValue();
			this.getView().byId("__table0").getBinding("items").filter(new sap.ui.model.Filter("CMO_PROJE", "Contains", sQuery));
		},
		onCreatePress: function(oEvent) {
			var that = this;
			var itemAdd = this.dialogAdd.getModel("item").oData;
			var modelC = this.getView().getModel("LoggingCreate");
			console.log(itemAdd);
			modelC.read("/AGH_TSLC(CMO_PRJV='" + itemAdd.CMO_PROJE + "',ISSUE_VAR='" + itemAdd.CMO_ISSUM + "',DATE_VAR02='" + itemAdd.A0CALDAY +
				"')/Results", {
					success: function(oData, response) {
						console.log("Read Success!", oData.results);
						var item = oData.results[0];
						console.log("Data to be put", itemAdd);
						modelC.update("/AGH_TSLCResults('" + item.ID.replace(" ", "%20") + "')", itemAdd, {
							"success": function() {
								console.log("success Create");
							},
							"error": function(error) {
								console.log("Fail", error);
								that.onErrorDialog(JSON.stringify(error));

							},
							"merge": false,
							"async": false,
							"refreshAfterChange": false
						});
					},
					error: function(error) {
						console.log(error);
					}
				});
			this.dialogAdd.close();
		},
		onApprovePress: function() {
			var list = this.getView().byId("__table0").getSelectedItems();
			var l = [];
			var that = this;
			var model = that.getView().getModel();
			// var batchOperations = [];
			list.forEach(function(x) {
				var i = x.getBindingContext().getProperty(x.getBindingContext().getPath());
				// model.setProperty(x.getBindingContext().getPath() + "/A0EP63AIXTZ482UMWIKN25MIPD", "0");
				// model.addBatchChangeOperations(model.                         );

				// console.log(x.getBindingContext().getPath())
				// console.log(i);
				// console.log(i.A0EP63AIXTZ482UMWIKN25MIPD)

				// console.log(i);
				if ((i.A0EP63AIXTZ482UMWIKN25MIPD === "0.000") || (i.A0EP63AIXTZ482UMWIKN25MIPD === "3.000")) {
					i.A0EP63AIXTZ482UMWIKN25MIPD = "1";
					l.push(i);
				}
			});
			// console.log(l);

			// model.submitChanges({
			// 	success: function(oData) {
			// 		console.log("Submit succesfull", oData);
			// 	},
			// 	error: function(oError) {
			// 		console.log("Submit Failed!", oError);
			// 		that.onErrorDialog(JSON.stringify(oError));
			// 	}
			// });
			this.getView().byId("__table0").setBusy(true);
			var PromiseList = l.map(function(item) {
				return new Promise(function(resolve, reject) {
					model.update("/AGH_TSHResults('" + item.ID.replace(" ", "%20") + "')", item, {
						"success": function() {
							console.log("Promise success");
							resolve();
						},
						"error": function(error) {
							console.log("Fail", error.responseText);
							that.onErrorDialog(JSON.stringify(error.responseText));
						},
						"merge": false,
						"async": false,
						"refreshAfterChange": false
					});
					// });
				});
			});

			Promise.all(PromiseList).then(function() {
				// model.submitChanges();
				console.log("Approval finished");
				// model.refresh();
				that.getView().byId("__table0").setBusy(false);

			});

		},
		onErrorDialog: function(oError) {
			console.log("in dialog", oError);
			var that = this;
			// var text = new sap.m.Text({
			// 	text: oError
			// 	});
			var text = new sap.ui.core.HTML({
				content: oError
			});
			if (!this.dialogErr) {
				this.dialogErr = new sap.m.Dialog({
					title: 'Error',
					type: 'Message',
					state: 'Error',
					content: text,
					beginButton: new sap.m.Button({
						text: 'OK',
						press: function() {
							that.dialogErr.close();
							location.reload();
						}
					})
				});
			}
			// this.dialogErr.addContent(text)
			this.dialogErr.open();

		},
		onGroup: function() {
			console.log("Refreshing");
			this.getView().getModel().refresh();
		}
	});
});