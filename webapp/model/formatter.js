sap.ui.define([], function() {

	return {
		hoursFormat: function(h){
			// console.log(parseInt(h))
			return parseInt(h)
		},
		itemType: function(status){
				status = parseInt(status);
				
			switch (status){
				case 0:
					return "Detail";
				case 3:
					return "Detail";
				default:
					return "Inactive";
			} 
		},
		approveIcon: function(icon) {
			icon = parseInt(icon);
			// console.log(icon)
			switch (icon) {
				case 0:
					return "sap-icon://user-edit";
				case 1:
					return "sap-icon://pending";
				case 2:
					return "sap-icon://accept";
				case 3:
					return "sap-icon://decline";
				default:
					return "sap-icon://error";
			}

		},
		approveIconColor: function(icon) {
			icon = parseInt(icon);
			switch (icon) {
				case 0:
					return "Warning";
				case 1:
					return "None";
				case 2:
					return "Success";
				case 3:
					return "Error";
				default:
					return "Error";
			}
		},
		approveIconText: function(icon) {
			icon = parseInt(icon);
			switch (icon) {
				case 0:
					return "Editable";
				case 1:
					return "Pending";
				case 2:
					return "Approved";
				case 3:
					return "Rejected";
				default:
					return "ERROR";
			}
		}

	};
});